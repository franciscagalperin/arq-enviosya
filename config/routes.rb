Rails.application.routes.draw do
  get 'receipt/show'

  get 'welcome/index'

  root 'welcome#index'

  get 'profile', to: 'users#show'

  get 'signup', to: 'users#new'
  get 'signup/:id', to: 'users#new'
  resources :users, path: 'signup', as: :users, only: [:create]
  resources :users, except: [:new, :show]

  get 'signup-delivery', to: 'deliveries#new'
  resources :deliveries, path: 'signup-delivery', as: :deliveries, only: [:create]
  resources :deliveries, except: [:new, :show]

  resources :shipments

  get 'login', to: 'sessions#new'
  post 'login', to: 'sessions#create'
  delete 'logout', to: 'sessions#destroy'

  get 'healthcheck', to: 'healthcheck#healthcheck'

  get 'receipts/:id', to: 'receipts#show'

  Rails.application.routes.draw do
    resources :delivery_locator_apis
  end

  Rails.application.routes.draw do
    resources :shipping_rates
  end

end
