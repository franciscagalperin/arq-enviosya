class UsersController < ApplicationController
   def new
      if(params[:id])
         @user = User.find_by_registration_code(params[:id])
         if ! @user
            flash[:danger] = "Código inválido"
           redirect_to welcome_index_path
         end
      else
         @user = User.new
      end
   end

   def update

      @user = User.find_by_id(params[:id])
      # only allow if it has a registration code and the email is the same
      if user_params[:email] == @user.email && @user.is_active == false && !@user.registration_code.nil? && !@user.registration_code.empty?
         @user.name     = user_params[:name]
         @user.lastname = user_params[:lastname]
         @user.document = user_params[:document]
         @user.password = user_params[:password]
         @user.profile_picture = user_params[:profile_picture]
         @user.role = 3
         @user.registration_code = nil

         if @user.save
            # activate the promotion for sender andreceiver
            discount = Discount.find_by_receiver_id(@user.id)
            discount.receiver_active = true
            discount.sender_active = true;
            discount.save

            flash[:success] = "Bienvenido #{@user.name}, tu cuenta fue creada con éxito"
            MyLog.debug "Se ha creado el usuario #{@user.id}"
            # logs the user in if it has role 3
            session[:user_id] = @user.id
            redirect_to profile_path(@user)
         end
      end

   end

   def create
      # New user
      @user = User.new(user_params)
      @user.role = 3
      if @user.save
         flash[:success] = "Bienvenido #{@user.name}, tu cuenta fue creada con éxito"
         MyLog.debug "Se ha creado el usuario #{@user.id}"
         # logs the user in if it has role 3
         session[:user_id] = @user.id
         redirect_to profile_path(@user)
      else
         render 'new'
      end
   end

   def show
      @user = current_user
      @page = "profile"
      render 'profile'
   end

   private def user_params
      params.require(:user).permit(:name, :lastname, :document, :email, :password, :profile_picture)
   end
end
