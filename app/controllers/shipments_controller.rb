class ShipmentsController < ApplicationController

  def new
    @shipment = Shipment.new
  end

  def create

     is_new_user = false
     receiver_email = params[:shipment][:receiver]
     receiver = User.find_by_email(receiver_email)

     if (!receiver)
       is_new_user = true

         #if the user doesn't exists, we have to create one and assign to the shipment
         # this will create a new user with an especial code for registration
         receiver = User.new
         receiver = receiver.createNewUserFromShipment receiver_email
         result = receiver.save

     end

    price = 100

     if is_new_user
       # create discount
       discount = Discount.new
       discount.sender = current_user
       discount.receiver = receiver
       discount.sender_active = false
       discount.receiver_active = true
       discount.save
       # send the email to the user who will receives the package
       UserMailer.welcome_email(receiver).deliver_now!
       MyLog.debug "Se ha creado el descuento #{discount.id}"
     end

     # Checks if the sender has a discount to be applied to the price
     has_discount = false
     # search for receiver discount
     discount_tobeused = Discount.find_by_receiver_id(current_user.id)
     if discount_tobeused != nil && discount_tobeused.receiver_active
       has_discount = true
       discount_tobeused.receiver_active = false
       discount_tobeused.sender_active = true
       discount_tobeused.save
     else
       # search for sender discount
       discount_tobeused = Discount.find_by_sender_id(current_user.id)
       if discount_tobeused != nil && discount_tobeused.sender_active
         has_discount = true
         discount_tobeused.sender_active = false
         discount_tobeused.save
       end
     end

       # Applies discount
       if has_discount
         price = price*0.50
         if @shipment.save
           flash[:success] = "Se ha solicitado el envío con un descuento del 50%!"
           MyLog.debug "Se ha aplicado un descuento del 50% al envío"
         end
       end

      #####################################################################
      begin
        url = 'http://arq-deliveries.mybluemix.net/shipments'
        response = RestClient.post url,
                                   {"shipment": {"sender":current_user.id,
                                              "receiver":receiver.id,
                                              "from_lat":params[:shipment][:from_lat],
                                              "from_long":params[:shipment][:from_long],
                                               "to_lat":params[:shipment][:to_lat],
                                               "to_long":params[:shipment][:to_long],
                                              "price":price,
                                               "delivery":Delivery.getClosestFree.user.id,

                                   }}.to_json,
                                   :content_type => "application/json"
     # rescue RestClient::Exception

      json_parsed = JSON.parse(response.body)
      @shipment = Shipment.new
      @shipment.id = json_parsed["data"]["id"]
      @shipment.user_id = json_parsed["data"]["delivery"]
      sender_id = json_parsed["data"]["sender"]
      @shipment.sender = User.find(sender_id)
      receiver_id = json_parsed["data"]["receiver"]
      @shipment.receiver = User.find(receiver_id)
      @shipment.from_long = json_parsed["data"]["from_long"]
      @shipment.from_lat = json_parsed["data"]["from_lat"]
      @shipment.to_long = json_parsed["data"]["to_long"]
      @shipment.to_lat = json_parsed["data"]["to_lat"]
      @shipment.price = json_parsed["data"]["price"]
      @shipment.status = json_parsed["data"]["status"]

      ###########################################################################


     flash[:success] = "Se ha solicitado el envío"
     MyLog.debug "Se ha solicitado el envío"
    redirect_to :action => :show, :id => json_parsed["data"]["id"]
    else
      flash[:alert] = 'No se ha podido crear el envío!'
      end
  end

  def show

    begin
      url = "http://arq-deliveries.mybluemix.net/shipments/#{params[:id]}"
      response = RestClient.get url,:content_type => "application/json"
      json_parsed = JSON.parse(response.body)
      @shipment = Shipment.new
      @shipment.id = json_parsed["data"]["id"]
      @shipment.user_id = json_parsed["data"]["delivery"]
      sender_id = json_parsed["data"]["sender"]
      @shipment.sender = User.find(sender_id)
      receiver_id = json_parsed["data"]["receiver"]
      @shipment.receiver = User.find(receiver_id)
      @shipment.from_long = json_parsed["data"]["from_long"]
      @shipment.from_lat = json_parsed["data"]["from_lat"]
      @shipment.to_long = json_parsed["data"]["to_long"]
      @shipment.to_lat = json_parsed["data"]["to_lat"]
      @shipment.price = json_parsed["data"]["price"]
      @shipment.status = json_parsed["data"]["status"]


      rescue RestClient::Exception

      if (@shipment != nil)
        if (current_user.id == @shipment.sender.id) || (current_user.id == @shipment.receiver.id) || (current_user.id == @shipment.delivery.id)
          render 'show'
        else
          flash[:danger] = "No tienes permisos para realizar la acción requerida"
          redirect_to root_path
        end
      else
        flash[:danger] = "No tienes permisos para realizar la acción requerida"
        redirect_to root_path
      end

    end
  end

  def index
    is_delivery?

    begin
      url = "http://arq-deliveries.mybluemix.net/shipments"
      response = RestClient.get url,:content_type => "application/json"
      json_parsed = JSON.parse(response.body)

      shipments_collection = []

      # acceder a data
      collection = json_parsed['data']

      # iterar sobre el set de resultados
      for row in collection do

        # instanciar cada envio
        shipment = Shipment.new

        #setear los datos
        shipment.id = row['id']
        shipment.user_id = row['delivery']

        sender_id = row['sender']
        shipment.sender = User.find(sender_id)
        receiver_id = row['receiver']
        shipment.receiver = User.find(receiver_id)

        shipment.from_long = row['from_long']
        shipment.from_lat = row['from_lat']
        shipment.to_long = row['to_long']
        shipment.to_lat = row['to_lat']
        shipment.price = row['price']
        shipment.status = row['status']
        shipment.date_created = row['date_created']
        shipment.date_delivered = row['date_delivered']
        shipment.signature = row['signature']

        #agregar a shipments_collection
        shipments_collection.push shipment

      end

      # pasar shipments_colleciton a la view
      @shipments = shipments_collection

    end
  end

  def update
    begin
      url = "http://arq-deliveries.mybluemix.net/shipments/#{params[:id]}"
      signature = params[:shipment][:signature]
      payload = JSON.generate("signature": signature)
      response = RestClient.put url,
                                payload,
                                 :content_type => "application/json"

      json_parsed = JSON.parse(response.body)
      @shipment = Shipment.new
      @shipment.id = json_parsed["data"]["id"]
      @shipment.user_id = json_parsed["data"]["delivery"]
      sender_id = json_parsed["data"]["sender"]
      @shipment.sender = User.find(sender_id)
      receiver_id = json_parsed["data"]["receiver"]
      @shipment.receiver = User.find(receiver_id)
      @shipment.from_long = json_parsed["data"]["from_long"]
      @shipment.from_lat = json_parsed["data"]["from_lat"]
      @shipment.to_long = json_parsed["data"]["to_long"]
      @shipment.to_lat = json_parsed["data"]["to_lat"]
      @shipment.price = json_parsed["data"]["price"]
      @shipment.status = json_parsed["data"]["status"]
      @shipment.signature = json_parsed["data"]["signature"]
      @shipment.date_delivered = json_parsed["data"]["date_delivered"]
      @shipment.date_created = json_parsed["data"]["date_created"]


    if @shipment.save
      flash[:success] = "Se ha entregado el envío #{@shipment.id}"
      MyLog.debug "Se ha entregado el envío #{@shipment.id}"
      #UserMailer.shipment_completed_email(@shipment).deliver_now!
      render 'show'
    end
end
  end

  private def shipment_params
    params.require(:shipment).permit(:sender, :receiver, :from_lat, :from_long, :to_lat, :to_long, :price, :status, :signature)
  end
end
