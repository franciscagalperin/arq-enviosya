class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  helper_method :current_user, :logged_in?

  def current_user
    # return the user id only if we have some value in session.
    # don't hit the database every time, hit only if we don't ask before
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end

  def logged_in?
    !!current_user
  end

  def require_user
    if !logged_in?
      flash[:danger] = 'No tienes permisos para realizar la acción requerida'
      redirect_to root_path
    end
  end

  def is_delivery?
    if !logged_in? || current_user.role != 2
      flash[:danger] = 'No tienes permisos para realizar la acción requerida'
      redirect_to root_path
    end
  end

end