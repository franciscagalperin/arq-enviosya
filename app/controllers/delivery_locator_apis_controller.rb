class DeliveryLocatorApisController < ApplicationController
  def index

    @deliveries = Delivery.all()
    render json: { status: 200,
                   data: @deliveries }

  end
end
