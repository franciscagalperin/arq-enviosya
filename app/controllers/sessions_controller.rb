class SessionsController < ApplicationController

  def new

  end

  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.is_active && user.authenticate(params[:session][:password])
      session[:user_id] = user.id
      flash[:success] = 'Has accedido al sistema correctamente'
      MyLog.debug "El usuario #{user.id} ha accedido al sistema correctamente"
      redirect_to profile_path(user)
    else
      flash.now[:danger] = 'Sus credenciales son inválidas o su perfil no se encuentra activo'
      MyLog.debug "Error de logeo: #{params[:session][:email]}"
      render 'new'
    end
  end

  def destroy
    MyLog.debug "#{session[:user_id]} ha cerrado la sesión"
    session[:user_id] = nil
    flash[:success] = "Has cerrado sesión correctamente"
    redirect_to root_path
  end
end