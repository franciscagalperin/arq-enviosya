class HealthcheckController < ApplicationController
  protect_from_forgery with: :null_session

  def healthcheck
    User.all
    Shipment.all
    render :status => 200
    return
  end

end