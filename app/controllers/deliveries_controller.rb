class DeliveriesController < ApplicationController
  def new
    @delivery = Delivery.new
  end

  def create
    @delivery = Delivery.new
    @delivery.documentation = user_params[:documentation]
    @delivery.driver_license = user_params[:driver_license]
    @delivery.available = true

    if @delivery.create_user(user_params[:user])
      @delivery.user.role = 2
      @delivery.user.is_active = false
      @delivery.user.save

      if @delivery.save
        flash[:success] = "#{@delivery.user.name}, su cuenta fue creada con éxito. Una vez aprobado podrá acceder al sistema."
        MyLog.debug "El cadete #{@delivery.user.id} se ha creado."
        redirect_to root_path
      else
        render 'new'
      end
    else
      render 'new'
    end
  end

  private def user_params
    params.require(:delivery).permit(:documentation, :driver_license, user: [:name, :lastname, :document, :email, :password, :profile_picture])
  end
end
