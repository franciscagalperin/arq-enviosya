class ReceiptsController < ApplicationController
  def show
    id = params[:id]
    @shipment = Shipment.find_by_id(id)
    if (current_user.id == @shipment.sender.id) || (current_user.id == @shipment.receiver.id) || (current_user.id == @shipment.delivery.id)
      render :pdf => "Recibo de Entrega # #{id}", :template => 'receipts/show.pdf.erb',  title: "Recibo de Entrega # #{id}"
    else
      flash[:danger] = 'No tienes permisos para realizar la acción requerida'
      redirect_to root_path
    end
  end
end