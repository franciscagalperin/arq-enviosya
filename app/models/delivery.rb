class Delivery < ActiveRecord::Base
  # associations
  belongs_to :user
  # needed for upload images, this maps the field with the handler
  mount_uploader :driver_license, PictureUploader
  mount_uploader :documentation, PictureUploader

  # validations for the User's model
  validates :driver_license, presence: true
  validates :documentation, presence: true
  validates_associated :user

  ## Here we can encapsulate the logic to pull the closest free delivery for the shipment.
  def self.getClosestFree
    # In this case we return one delivery without any implicit order just for test purpose
    available_deliveries = Delivery.find_by_available(true)
    return available_deliveries
  end
end
