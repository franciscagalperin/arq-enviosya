class Shipment < ActiveRecord::Base

   STATUS = {:IN_COURSE => "En curso", :DELIVERED => "Entregado"}
   
   # associations
   belongs_to :sender, class_name: 'User'
   belongs_to :receiver, class_name: 'User'
   belongs_to :delivery, class_name: 'User', foreign_key: :user_id

   # needed for upload images, this maps the field with the handler
   mount_uploader :signature, PictureUploader

   # validations for the Shipment's model
   validates :from_lat, presence: true
   validates :from_long, presence: true
   validates :to_lat, presence: true
   validates :to_long, presence: true

   ### Here you can place your price logic that will consume the price for an external api
   def calculatePrice
      # at this point we only return a random value between 100 and 1000
      price = Random.new
      return price.rand(100...1000)
   end

end
