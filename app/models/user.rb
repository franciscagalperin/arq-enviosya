class User < ActiveRecord::Base

  has_one :delivery
  # needed for upload images, this maps the field with the handler
  mount_uploader :profile_picture, PictureUploader

  # used for password encryption and provide methods to
  # check if the passwords match.
  has_secure_password

  # Regular expression for email
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i

  # Regular expression for document
  VALID_DOCUMENT_REGEX = /(\d{7})+-(\d{1})/

  # validations for the User's model
  validates :name, presence:true
  validates :lastname, presence:true
  validates :email, presence:true, uniqueness: { case_sensitive:false }, format: { with: VALID_EMAIL_REGEX }
  validates :document, presence:true, format: { with: VALID_DOCUMENT_REGEX }


  def createNewUserFromShipment(email)

    user = User.new
    user.email = email
    user.name = "User"
    user.lastname = "Temporary"
    user.document = "1111111-1"
    user.role = 3
    user.is_active = false
    user.registration_code = SecureRandom.hex
    user.password = "qwe123"

    return user
  end

end