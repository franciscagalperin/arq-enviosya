class ApplicationMailer < ActionMailer::Base
  default from: 'franciscagalperin@gmail.com'
  layout 'mailer'
end
