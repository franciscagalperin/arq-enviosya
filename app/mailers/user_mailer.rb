class UserMailer < ApplicationMailer
    default from: 'notifications@example.com'
    def welcome_email(user)
        @user = user
        @url  = 'http://arq-enviosya.mybluemix.net/signup/' + user.registration_code
        mail(to: @user.email, subject: 'Ha sido invitado a enviosYa')
    end

    def shipment_completed_email(shipment)
        @shipment = shipment
        mail(to: @shipment.receiver.email, subject: 'EnvíosYA - Envío entregado!')
    end
end
