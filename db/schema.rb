# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171029003701) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "deliveries", force: :cascade do |t|
    t.bigint "user_id"
    t.string "driver_license"
    t.string "documentation"
    t.float "location_lat"
    t.float "location_long"
    t.boolean "available"
    t.index ["user_id"], name: "index_deliveries_on_user_id"
  end

  create_table "discounts", force: :cascade do |t|
    t.boolean "is_active", default: true
    t.integer "receiver_id"
    t.integer "sender_id"
    t.boolean "receiver_active"
    t.boolean "sender_active"
  end

  create_table "shipments", force: :cascade do |t|
    t.bigint "sender_id"
    t.bigint "receiver_id"
    t.integer "price"
    t.float "from_lat"
    t.float "from_long"
    t.float "to_lat"
    t.float "to_long"
    t.string "status"
    t.bigint "user_id"
    t.string "signature"
    t.datetime "date_created"
    t.datetime "date_delivered"
    t.index ["receiver_id"], name: "index_shipments_on_receiver_id"
    t.index ["sender_id"], name: "index_shipments_on_sender_id"
    t.index ["user_id"], name: "index_shipments_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "lastname"
    t.string "document"
    t.string "profile_picture"
    t.string "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "password_digest"
    t.integer "role"
    t.boolean "is_active", default: true
    t.string "registration_code"
  end

  add_foreign_key "discounts", "users", column: "receiver_id"
  add_foreign_key "discounts", "users", column: "sender_id"
  add_foreign_key "shipments", "users"
end
