class AddDeliveryToShipment < ActiveRecord::Migration[5.1]
  def change
    add_reference :shipments, :delivery, class_name: 'User', foreign_key: true
  end
end
