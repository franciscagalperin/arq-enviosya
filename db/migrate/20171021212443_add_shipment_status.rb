class AddShipmentStatus < ActiveRecord::Migration[5.1]
  def change
    add_column :shipments, :status, :varchar
  end
end
