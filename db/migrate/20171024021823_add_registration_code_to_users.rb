class AddRegistrationCodeToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :registration_code, :string, null: true
  end
end
