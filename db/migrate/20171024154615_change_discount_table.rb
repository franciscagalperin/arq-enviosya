class ChangeDiscountTable < ActiveRecord::Migration[5.1]
  def change
    remove_column :discounts, :owner_id
    add_column :discounts, :receiver_id, :integer
    add_column :discounts, :sender_id, :integer
    add_column :discounts, :receiver_active, :boolean
    add_column :discounts, :sender_active, :boolean
    add_foreign_key :discounts, :users, column: :receiver_id
    add_foreign_key :discounts, :users, column: :sender_id
  end
end
