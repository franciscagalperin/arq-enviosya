class AddCreationAndDeliveryDatetime < ActiveRecord::Migration[5.1]
  def change
    add_column :shipments, :date_created, :datetime
    add_column :shipments, :date_delivered, :datetime
  end
end
