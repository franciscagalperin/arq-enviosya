class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :name
      t.string :lastname
      t.string :document
      t.string :profile_picture, null: true
      t.string :email
      t.timestamps 
    end
  end
end
