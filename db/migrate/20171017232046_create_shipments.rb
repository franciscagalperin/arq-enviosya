class CreateShipments < ActiveRecord::Migration[5.1]
  def change
    create_table :shipments do |t|
      t.belongs_to :sender, class_name: 'User'
      t.belongs_to :receiver, class_name: 'User'
      t.string :from
      t.string :to
      t.integer :price
    end
  end
end
