class AddSignatureToShipment < ActiveRecord::Migration[5.1]
  def change
    add_column :shipments, :signature, :string
  end
end
