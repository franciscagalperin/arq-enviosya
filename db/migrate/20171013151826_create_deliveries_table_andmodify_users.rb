class CreateDeliveriesTableAndmodifyUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :deliveries do |t|
      t.belongs_to :user, index: true
      t.string :driver_license
      t.string :documentation
    end

    # add inactive column on users
    add_column :users, :is_active, :boolean, default: true
  end
end
