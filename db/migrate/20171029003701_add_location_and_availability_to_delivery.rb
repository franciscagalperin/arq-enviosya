class AddLocationAndAvailabilityToDelivery < ActiveRecord::Migration[5.1]
  def change
    add_column :deliveries, :location_lat, :float
    add_column :deliveries, :location_long, :float
    add_column :deliveries, :available, :boolean
  end
end
