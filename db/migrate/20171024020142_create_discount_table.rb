class CreateDiscountTable < ActiveRecord::Migration[5.1]
  def change
    create_table :discounts do |t|
      t.belongs_to :owner, class_name: 'User'
      t.boolean :is_active, default: true
    end
  end
end
