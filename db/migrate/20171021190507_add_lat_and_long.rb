class AddLatAndLong < ActiveRecord::Migration[5.1]
  def change
    remove_column :shipments, :from
    remove_column :shipments, :to
    add_column :shipments, :from_lat, :float
    add_column :shipments, :from_long, :float
    add_column :shipments, :to_lat, :float
    add_column :shipments, :to_long, :float
  end
end
