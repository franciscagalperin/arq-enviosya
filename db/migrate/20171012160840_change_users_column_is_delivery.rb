class ChangeUsersColumnIsDelivery < ActiveRecord::Migration[5.1]
  def change
    remove_column :users, :is_delivery
    add_column :users, :role, :integer
  end
end
