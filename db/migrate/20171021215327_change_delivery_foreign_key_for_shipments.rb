class ChangeDeliveryForeignKeyForShipments < ActiveRecord::Migration[5.1]
  def change
    remove_column :shipments, :delivery_id
    add_reference :shipments, :user, class_name: 'User', foreign_key: true
  end
end
