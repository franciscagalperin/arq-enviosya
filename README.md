## Synopsis

This is a software created for a shipping company called 'EnviosYa'. 

## Motivation

The aim of the project is to supply an easy way of shipping objects between users through cadets.

## Installation in Development environment

1. If you don't have the project in your pc, you should download it.
2. Open Ruby Mine.
3. Migrate the local database with the command:
		rake db:migrate
4. Run the project.
5. Open the browser and enter the url: localhost:3000
6. Start working on Ruby Mine and test it (using the same url).

## Deployment in production - Bluemix

When you have changes to push in production:
1.   Migrate the remote database with the command:
	bx cf push -c “bundle exec rake db:migrate” arq-enviosya
2. Deploy in Bluemix with the command:
	bx cf push -b https://github.com/cloudfoundry/ruby-buildpack.git -c ‘rails server -p $PORT’ $APP_NAME 


## Stress Tests 

The stress tests will be runned with the JMeter tool. For doing it, it is necessary to:
1. Configure the browser according to the configuration of the HTTP Proxy server of JMeter.:
This includes changing the proxy settings, on Manual proxy configurations, adding: HTTP Proxy: localhost, port:8383.
2. Open the test plan and run it directly. 
3. The results will be displayed in a report.

